'use strict';

module.exports = {
  url: `http://www.sagesclub.surge.sh`,
  description: `In this have the Knowledge of how to work as a system manager and a developer`,
  pathPrefix: '/',
  title: `Sage's Club`,
  disqusShortname: '',
  postsPerPage: 15,
  useKatex: false,
  author: {
    name: `Samuel Dantas`,
    summary: `"Building the window and connecting worlds!."`,
    social: {
      twitter: ``,
      github: `tatarotus`,
      email: `samuelbdantas.pm@gmail.com`,
      linkedin: ``,
      facebook: ``,
      instagram: ``,
    },
  }
};
