---
title: Arch Linux Installation Guide
date: "2021-09-15"
template: post
---

#### Requirements: 
- Arch Linux ISO file
- Computer
- Internet connection (ethernet)
- USB Drive
- Some Linux Knowledge

### Bootable USB (usingi a ISO image)
Go to: [Archlinux.org/download](https://archlinux.org/download/) and download a ISO image file using a torrent client. I personally recommend Deluge web client (localhost:8112, password: deluge). The ISO is _Aprox.: ~800MB_

#### Plug USB drive in your device

	~$ lsblk
	NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
	sda      8:0    0 111.8G  0 disk
	├─sda1   8:1    0   200M  0 part /boot
	├─sda2   8:2    0     9G  0 part [SWAP]
	├─sda3   8:3    0    20G  0 part /
	└─sda4   8:4    0  82.6G  0 part /home
	sd[b]
	sd[c]
	sd[d] ... etc...

With the ___lsblk you can find ind the correct  **sd[tag]** to your USB device. For example:
```
/dev/sdc
```

#### Use the dd command
```bash
	dd if=[ISO file path] of=[USB Drive path] status="progress"
```
	
After the dd command writes you USB stick you are able to boot in BIOS and start the installation of your distro!

**Important step:**
Check UEFI or BIOS
```bash
		ls /sys/firmware/efi/efivars/
```

if empty it is BIOS if not it is UEFI! **This installation process is for a simple BIOS**

### Arch Linux Installation

Boot your pendrive you will be welcomed if a prompt screen:

![Prompt Screen](./arch_bash.png)

#### Check Internet && Time
You could check if your ethernet connection is working using the ping command.
If everything is working you should receive something like "packets transmited"
```bash
ping www.somewebisite.com
```

_Set date using_ timedatectl command like the example below:
```bash
timedatectl set-ntp true
```

#### Drive Partitioning
Check the drive you and to format and start a flesh installation of your distro using the _lsblk_ program. Please be sure to select the right drive. You are about to format at this step!

**Partitioning with fdisk**:

Enter in the _fdisk_ utility 
```bash
$ fdisk /dev/sd[tag]
```

the commands that we're gonna use is:

**p** - _print table_

**d** - _delete partition_

**n** - _add new partition_

**w** - _write changes in disk_

You could always check the list of fdisk commands by typing "m"

![prompt screen in fdisk program](./arch_fdisk.png)
The example above show us how to create a boot partition. It's basically the same for all other partitions except for the size.


_A common way to setup Arch linux partitions:_

| Number | Partition   | Space    			  |
| :----- | :---------- | -------------------: |
| 1      | Boot        | ~ 200M   			  |
| 2      | Swap        | 150% RAM 		      |
| 3      | Root        | 20G ~ 25G   		  |
| 4      | Home        | + 80G (space left)   |


01) Delete all partitions in the disk
02) Partition the disk in 4 parts
03) Write changes in disk
04) Use mkfs and create storage ext4 in the boot, root and home partition (exept swap)

#### Creating filesystems in ext4 formats with mkfs
```bash
$ mkfs.ext4 /dev/sd[tag]1 # Boot
$ mkswap /dev/sd[tag]2    # Swap
$ mkfs.ext4 /dev/sd[tag]3 # Root
$ mkfs.ext4 /dev/sd[tag]4 # Home
$ swapon /dev/sd[tag]2    
```
**Why Ext4?**

>Ext4 is one of the latest and greatest Linux file formats.
>Ext4 modifies important data structures of the filesystem such as the ones destined to store the file data.
>The ext4 format allows users to still read the filesystem from other distributions/operating systems without ext4 support.
>Ext3/4 is by far the best filesystem format, but it's not supported natively by Windows or Macs. 
>ext4 has very large limits on file and partition sizes., allowing you to store files much larger than the 4 GB allowed by FAT32.

[Read more about this topic here](https://www.geekboots.com/story/ntfs-vs-ext4-vs-fat32)


#### Mounting

Now we need to mount the boot & home in the root to later have a filesystem like this:
```
bin  boot  dev  etc  home  lib  lib64  lost+found  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
```
The boot and home folder we're going to add manually. Everything in linux is a file or folder, even programs. So we are going to setup the distro folder structure later with the _pacstrap_ command.

Let's mount in a proper order!
```bash
mount /dev/sd[tag]3 /mnt

mkdir /mnt/home
mkdir /mnt/boot

mount /dev/sd[tag]1 /mnt/boot
mount /dev/sd[tag]4 /mnt/home
```

So we now have something similar to this mount and partitioning structure. _It is a virtual machine example. That's why the partition size is so low..._

![lsblk result](./arch_lsblk.png)

### Installing Arch Distribution
	$ pacstrap -i /mnt base

#### FSTAB (to save mount configuration)
>Your Linux system's filesystem table, aka fstab, is a configuration table designed to ease the burden of mounting and unmounting file systems to a machine. It is a set of rules used to control how different filesystems are treated each time they are introduced to a system.

Filesystems, and by necessity, filesystem tables can be a bit tricky, so we will look at _/etc/fstab_ and generate this file using a simple command for our mount setup always be remembered like it is now. _It is important to tag by UIID because block paths can change_
```
genfstab -U /mnt >> /mnt/etc/fstab
```


#### Enter in Arch Command (chroot)

Now we are leave the ISO and enter in your brand-new installation.
The installation is almost done this is our distro we could even use pacman and have access to all the packages available. We still need to install the linux kernel and configure the images(initramfs) for grub to boot. But look at it we've come that far... Congratulations!
```
$ arch-chroot /mnt
```

### Install linux kernel, Network & Basic Configuration
```bash
$ pacman -S base-devel
$ pacman -S linux linux-headers
```

#### Install Network Packages
```bash
$ pacman -S networkmanager
$ systemctl enable NetworkManager # Capitalized
```

#### Install LVM and configure GRUB
>LVM stands for Logical Volume Management. It is a system of managing logical volumes, or filesystems, that is much more advanced and flexible than the traditional method of partitioning a dis
>You can create/resize/delete LVM "partitions" (they're called "Logical Volumes" in LVM-speak) from the command line while your Linux system is running: no need to reboot the system to make the kernel aware of the newly-created or resized partitions.

```zsh
$ pacman -S lvm2
$ pacman -S grub dosfstools os-prober mtools 
$ pacman -S vim # editor of your choice
```
```bash
$ vim /etc/mkinitcpio.conf
```
Find the first uncommented HOOKS line and add the lvm2 support between **block** and **filesystems**.
Your HOOK line should be something like this:

_HOOKS=(base udev autodetect modconf block **lvm2** filesystems keyboard fsck)_

and then run:
```bash
mkinitcpio -p linux
```
change linux for whatever kernel you installed
```zh
$ grub-install --target=i386-pc --recheck /dev/sd[tag] # target the whole drive not partition
$ grub-mkconfig -o /boot/grub/grub.cfg
```
#### Configuration & Security & Location

```
vim /etc/locale.gen
```
and uncomment the line with your locale (en_US.UTF-8 UTF-8)
then run 
```
locale-gen
```
```
vim /etc/locale.conf
```
and add "LANG=en_US.UTF-8"

```
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
```

##### Create a root password
	passwd
##### Create a user, add a home dir and add to sudo group (wheel)
```zsh
useradd -m -g users -G wheel [username]
visudo #uncomment the wheel line
passwd [username]
```
##### Hostname
hostnamectl set-hostname [name]

##### CPU micromanager
```bash
$ pacman -S intel-ucode
```
#### Exit & Umount
```
	exit
	umount -R /mnt
```

## Done!

This should install Arch Linux in your system!

>a big thanks to [Luke Smith](https://www.youtube.com/watch?v=4PBqpX0_UOc&t=1965s) & [Learn Linux TV](https://www.youtube.com/watch?v=DPLnBPM4DhI&t=10s)