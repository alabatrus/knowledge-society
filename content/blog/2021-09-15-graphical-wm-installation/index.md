---
title: Graphical Windom Manager Installation
date: "2021-09-23"
template: post
---

#### Requirements: 
- Arch Linux Distro with pacman
- 500MB or more of RAM
- Internet connection 
- Some Linux Knowledge

### Suckless philosophy	
>Our philosophy is about keeping things simple, minimal and usable. We believe this should become the mainstream philosophy in the IT sector. Unfortunately, the tendency for complex, error-prone and slow software seems to be prevalent in the present-day software industry. We intend to prove the opposite with our software projects.

>Our project focuses on advanced and experienced computer users. In contrast with the usual proprietary software world or many mainstream open source projects that focus more on average and non-technical end users, we think that experienced users are mostly ignored. This is particularly true for user interfaces, such as graphical environments on desktop computers, on mobile devices, and in so-called Web applications. We believe that the market of experienced users is growing continuously, with each user looking for more appropriate solutions for his/her work style.

#### Install xorg

```bash
$ sudo pacman -S xorg-server xorg-xinit xorg-xsetroot
```
**if you have an intel GPU you should install mesa**
```bash
$ pacman -S mesa
```
#### Install dwm (dinamic window manager) & st (simple terminal)
```
	$ sudo pacman -S unzip
	$ sudo pacman -S wget
	$ wget https://gitlab.com/alabatrus1/suckless/-/archive/main/suckless-main.zip
	$ unzip suckless-main.zip
```

You should have 2 folders inside suckless-main uncompressed by unzip
dwm & st

let's install both running _make_ inside each folder:
#### DWM & Dmenu & Dwmbar(custom shell script)
Before running the install try to add these libraries
```
$ pacman -S libxft
$ pacman -S libxinerama
```
then: 

```
$ sudo make clean install
$ sudo pacman -S dmenu
```


#### DWM BAR

Download the dwmbar script [here](https://gitlab.com/alabatrus/dotfiles/-/raw/main/dwmbar?inline=false) and put it in /usr/local/bin
Uncomment the line in .xinitrc to execute this script at startup
#### ST 
You should not have to install any libraries to run and install _st_ simply run:
```
$ sudo make clean install
```
### .xinitrc
>The xinit program is used to start the X Window System server and a first client program on systems that cannot start X directly from /etc/init or in environments that use multiple window systems. When this first client exits, xinit will kill the X server and then terminate.
After the dd command writes you USB stick you are able to boot in BIOS and start the installation of your distro!

We need to configure this file is to autostart dwm in logon as well xrandr for fixing the display resolution, nitrogen for wallpaper and picon to render transparency. Let's start by downloading the needed packages

```bash
$ sudo pacman -S nitrogen picom  
```
then we need to configure the xinitrc. Let's copy from our X11 directory and move it to our home

```bash
cp /etc/X11/xinit/xinitrc .xinitrc
```
In your xinitrc file, remove the twm& block (the last 5 lines)

and let's add your custom configurations in the last lines
```bash
#Keyboard Layout
setxkbmap us &


# Compositor
picom -f &

# Wallpaper
nitrogen --restore &


#Display Resolution
xrandr --newmode "1280x1024_60.00"  108.88  1280 1360 1496 1712  1024 1025 1028 1060  -HSync +Vsyn
xrandr --addmode VGA-1 1280x1024_60.00
xrandr -s 1280x1024
xrandr --auto && xrandr --output LVDS-1 --off


# Dwm custom bar
#zsh dwmbar 2>&1 >/dev/null & enable after installing zsh and creating the script

#Execute DWM
exec dwm
```
**OBS:**
To create your custom display resolution you can use the _cvt_
for example: _|cvt width height fps|_
```
cvt 1680 1050 60
```

Before running _startx_ It's necessary to install dwm fonts in Arch we have a package called noto fonts it install some google fonts into your system

```bash
$ pacman -S noto-fonts
$ pacman -S ttf-font-awesome 
```

then:
**startx**


### Installing zsh and ohmyzsh shell
```bash
sudo pacman -S zsh
```
```
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

[Download here the smre.zsh-theme](https://gitlab.com/alabatrus/dotfiles/-/raw/main/smre.zsh-theme?inline=false)

**Make it default**
```
$ chsh -s /bin/zsh
```
then reboot to apply changes



### Install YAY AUR 
```
$ cd /opt
$ sudo git clone https://aur.archlinux.org/yay-git.git
```
Change the file permissions from the root the sudo user.
```
sudo chown -R [user]:[usergroup] ./ya	y-git
```
To build the package from PKGBUILD, navigate into the yay folder.
```
$ cd yay-git
```
Next, build the package using the makepkg command below.
```
$ makepkg -si
```

#### Using YAY
Once you have yay installed, you can upgrade all the packages on your system using the command.
```
$ sudo yay -Syu
```
To include development packages during the upgrade run.
```
$ yay -Syu --devel --timeupdate
```
As with any other AUR helpers, you can install the packages using the command.
```
$ sudo yay -S gparted
```
To remove a package using yay use the command.
```
$ sudo yay -Rns package_name
```
To clean up all unwanted dependencies on your system, issue the command.

$ sudo yay -Yc
If you want to print system statistics using yay, run.
```
$ sudo yay -Ps
```

[Learn more here](https://www.tecmint.com/install-yay-aur-helper-in-arch-linux-and-manjaro/)


### Installing a web browser

```
$ yay -S brave-bin
```	


#### Adding a Wallpaper
In you home directory create Pictures/wallpapers folders

Run nitrogen from the terminal or dmenu, add the wallpaper folder and apply your wallpaper in the xinitrc file add the command

```
nitrogen --restore &
```

### Adding Custom Vim configuration

_You should use and learn SPACE VIM_

[Go to space vim  website](https://spacevim.org/quick-start-guide/)

or paste this command in your terminal
```
curl -sLf https://spacevim.org/install.sh | bash
```
It will automatically install the custom configuration and plugins for VIM

